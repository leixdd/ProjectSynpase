/*
 * The MIT License
 *
 * Copyright 2019 BlackMoon.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package synapse;

import java.util.HashMap;
import synapse.core.db.Providers;

/**
 *
 * @author BlackMoon
 */
public class Config {

    /*======================
    * Start Configuration Here
    * ======================*/
    //======================
    //Database Configuration
    //======================
    protected HashMap<String, Object> database = new HashMap<>();

    public Boolean setDatabase(Providers provider, String Host, String Port, String Database, String Username, String password) {
        if (!provider.equals(Providers.MSSQL)) {
            this.database.putIfAbsent("provider", provider);
            this.database.putIfAbsent("host", Host);
            this.database.putIfAbsent("port", Port);
            this.database.putIfAbsent("database", Database);
            this.database.putIfAbsent("username", Username);
            this.database.putIfAbsent("password", password);
            return true;
        }
        
        System.err.println("DB Configurations Error: Please set a schema");
        return false;
    }

    public Boolean setDatabase(Providers provider, String Host, String Port, String Database, String Username, String password, String Schema) {
        this.database.putIfAbsent("provider", provider);
        this.database.putIfAbsent("schema", Schema);
        this.database.putIfAbsent("host", Host);
        this.database.putIfAbsent("port", Port);
        this.database.putIfAbsent("database", Database);
        this.database.putIfAbsent("username", Username);
        this.database.putIfAbsent("password", password);
        
        return true;
    }

    //======================
    // End DB Configuration
    //======================
    /*======================
    * Don't Mind this Part
    * Set up for Singleton
    * ======================*/
    private Config() {
    }

    protected HashMap<String, HashMap<String, Object>> hash = new HashMap<>();

    private static class ownConfig {

        private static Config cfg = new Config();
    }

    public static Config getInstance() {
        return ownConfig.cfg;
    }

    public HashMap<String, HashMap<String, Object>> getConfigurations() {
        this.hash.put("database", this.database);
        return this.hash;
    }

}
