/*
 * The MIT License
 *
 * Copyright 2019 BlackMoon.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package synapse.core.db;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BlackMoon
 */
public class DB_Engine {

    private IDatabase PROVIDER_INSTANCE;

    //Singleton
    private DB_Engine() {
    }

    private static class init {

        private static final DB_Engine engine = new DB_Engine();
    }

    public static DB_Engine _i() {
        return init.engine;
    }

    public IDatabase setProvider(String db) {

        try {
            this.PROVIDER_INSTANCE = (IDatabase) Class.forName("synapse.core.db.providers." + db.toUpperCase()).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
            Logger.getLogger(DB_Engine.class.getName()).log(Level.SEVERE, null, ex);
        }

        return this.PROVIDER_INSTANCE;
    }

    public IDatabase getProvider() {
        return this.PROVIDER_INSTANCE;
    }

}
