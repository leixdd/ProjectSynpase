/*
 * The MIT License
 *
 * Copyright 2019 BlackMoon.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package synapse.core.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BlackMoon
 */
public class Database implements IDatabase {

    private Connection connection;
    private String host = "";
    private String ConnectionString = "";
    
    public void setHost(String host) {
        this.host = host;
    }

    public void setConnectionString(String ConnectionString) {
        this.ConnectionString = this.host + ConnectionString;
    }
   

    @Override
    public Connection getConnection() {
        return this.connection;
    }

    @Override
    public Boolean hasConnection() {
        try {
            return this.connection != null && !this.connection.isClosed();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return false;
    }

    @Override
    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void applyConfig() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean startConnection() {

        try {

            this.connection = (Connection) DriverManager.getConnection(this.ConnectionString);
            System.out.println("synapse.core.db.Database.startConnection() : Connection was started");
            return true;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }

        return false;
    }

}
